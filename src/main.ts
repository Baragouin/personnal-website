import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import { createPinia } from 'pinia'

import VueCookies from 'vue-cookies'

import App from './App.vue'
import router from './router'

import { library } from '@fortawesome/fontawesome-svg-core'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faCloudUploadAlt,
  faDesktop,
  faDiagramProject,
  faGlobe,
  faServer,
  faUsers
} from '@fortawesome/free-solid-svg-icons'

import {
  faLinkedin,
  faTwitter,
  faDiscord,
  faGithub,
  faGitlab
} from '@fortawesome/free-brands-svg-icons'

import './assets/main.scss'
import fr from './locales/fr.json'
import en from './locales/en.json'

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages: { fr, en }
})

const app = createApp(App)

app.use(VueCookies, { expires: '7d' })
app.use(createPinia())
app.use(router)
app.use(i18n)

app.mount('#app')
app.component('font-awesome-icon', FontAwesomeIcon)

library.add(faDiagramProject)
library.add(faUsers)
library.add(faLinkedin)
library.add(faTwitter)
library.add(faDiscord)
library.add(faGithub)
library.add(faGitlab)
library.add(faGlobe)
library.add(faDesktop)
library.add(faServer)
library.add(faCloudUploadAlt)
